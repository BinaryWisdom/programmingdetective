# ProgrammingDetective

This program checks C++ files in a target directory for common bad programming practices.
 
Interface is implemented using "naked" Windows API. Windows API was chosen for purely educational purposes.

The whole program is intended for personal use only and doesn't deal with scenarios and situations not encountered by the author.
