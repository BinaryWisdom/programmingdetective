#ifndef _PCRIMESLIST_H
#define _PCRIMESLIST_H

#include "PCrime.h"
#include <vector>
#include <string>
#include <set>
#include <unordered_set>

enum class Token_Type { TYPENAME, IDENTIFIER, OTHER };

class PCrimesList
{
public:
	PCrimesList() = default;
	PCrimesList(const PCrimesList &origin);
	PCrimesList(PCrimesList &&origin);
	PCrimesList & operator=(const PCrimesList &rhs);
	PCrimesList & operator=(PCrimesList &&rhs);

	PCrimesList(const std::wstring & filename);
	int FindCrimesInFile(const std::wstring & filename);
	int GetNumberOfCrimes() const;
	std::set<int> GetCrimeLinesNumbers() const;

private:
	std::vector<PCrime> crimes;

	std::unordered_set<std::string> datatypes;
	std::unordered_set<std::string> non_datatypes_keywords;
	std::unordered_set<char> separators;

	int line_number{ 0 };
	int opened_curly{ 0 };

	Token_Type Get_Token_Type(const std::string & token) const;
	inline std::string Next_Token(std::ifstream & input_file);
	inline void SkipCurrentStatement(std::ifstream & input_file);
	void Initialize();

	friend std::ostream & operator<< (std::ostream & str, const PCrimesList & rhs);
	friend std::wostream & operator<< (std::wostream & str, const PCrimesList & rhs);
};

#endif