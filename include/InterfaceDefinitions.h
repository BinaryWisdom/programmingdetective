#ifndef _INTERFACE_DEFINITIONS_H
#define _INTERFACE_DEFINITIONS_H

constexpr int BROWSE_ID = 1;
constexpr int RUN_TEST_ID = 2;
constexpr int NEW_PATH_ID = 3;
constexpr int SELECT_FILE_ID = 4;
constexpr int NEXT_SUSPECT_ID = 5;
constexpr int PREV_SUSPECT_ID = 6;

constexpr int MAIN_MIN_WIDTH = 800;
constexpr int MAIN_MIN_HIEGHT = 600;

constexpr int CONTENT_X_POS = 400;
constexpr int CONTENT_Y_POS = 50;
constexpr int CONTENT_X_DISTANCE_TO_MAIN_RIGHT = 30;
constexpr int CONTENT_Y_DISTANCE_TO_MAIN_BOTTOM = 57;
constexpr int CONTENT_DISTANCE_BETWEEN_LINES = 20;
constexpr int CONTENT_FIRST_LINE_POSITION = 5;
constexpr int CONTENT_LINES_PER_DELTA_SCROLL = 3;

constexpr int BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM = 42;
constexpr int NEXT_X_DISTANCE_TO_MAIN_RIGHT = 227;
constexpr int PREV_X_DISTANCE_TO_MAIN_RIGHT = 30;

constexpr int FILES_X_POS = 30;
constexpr int FILES_Y_POS = 120;
constexpr int FILES_X_WIDTH = 350;
constexpr int FILES_Y_DISTANCE_TO_MAIN_BOTTOM = 57;

constexpr int MAX_LINE_LENGTH = 1024;

#endif
