#ifndef _FSA_H
#define _FSA_H

#include <string>
#include "PCrime.h"
#include "PCrimesList.h"



class FSA
{
public:
	virtual void Reset() = 0;
};


class GotoFSA :public FSA
{
public:

	bool InputToken(const std::string & token);
	void Reset() {};
};


class VLAandGlobalFSA :public FSA
{
public:

	bool InputToken(const std::string & token, Token_Type type, PCrime_Type &crime_type);
	void Reset();

private:

	enum class FSA_State {
		INIT, TYPE_NAME, VAR_NAME, SQUARE_BR_OPENED,
		IDENT_IN_BR, ANGLE_BR_OPENED, PARENTH_OPENED,
		SMTHG_IN_PARENTH, MANY_IN_PARENTH
	} state_FSA{ FSA_State::INIT };

	int opened_curly{ 0 };

};

#endif
