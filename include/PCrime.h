#ifndef _PCRIME_H
#define _PCRIME_H

#include <string>
#include <ostream>

enum class PCrime_Type {GOTO, GLOBAL, VLA, UNDEFINED};

class PCrime
{
public:
	PCrime() = default;
	PCrime(PCrime_Type n_type, int n_line);
	
	void Set_PCrime(PCrime_Type n_type, int n_line);
	PCrime_Type Get_Type() const;
	int Get_Line() const;

private:
	PCrime_Type type;
	int found_in_line{ -1 };

	friend std::ostream & operator<< (std::ostream & str, const PCrime & rhs);
};

#endif