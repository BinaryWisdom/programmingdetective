#ifndef _TASKSSTORAGE_H
#define _TASKSSTORAGE_H

#include "../include/PCrime.h"
#include "../include/PCrimesList.h"
#include <windows.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <mutex>
#include <thread>

#define NUM_OF_RESERVED_THREADS 2 // Program tries to reserve that many threads for other computer tasks

class DetectiveEngine
{
public:
	DetectiveEngine();
	int CollectTasks(LPCWSTR folder); // Finds all C/C++ source files in a specified folder and its subfolders 
	void InvestigateTasks(); // Find programming crimes in files
	void LogResultToFile(std::string filename) const;
	int GetNumberOfTasks() const;
	std::vector<std::wstring> GetSuspiciousFilesList() const;
	std::set<int> GetSuspiciousLinesList(std::wstring filename) const;
	void Clear();

private:
	std::vector<std::wstring> files;
	
	std::map<std::wstring, PCrimesList> found_crimes_storage;
	std::mutex storage_lock;
	unsigned int current_job_index{ 0 };
	std::mutex current_job_lock;

	unsigned short max_num_of_threads;

	
	enum struct FileTypes {INTERESTING, UNINTERESTING, FOLDER};

	DetectiveEngine::FileTypes CheckFilename(LPWIN32_FIND_DATAW file); // Checks if the file is a C++ file or folder
	void SingleFileAnalysis(std::wstring filename); // Function to analyze a single file and record the results in storage
	void WorkerThreadFunction(); //represents a single worker thread
};








#endif
