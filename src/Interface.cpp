#include "../include/DetectiveEngine.h"
#include "../include/InterfaceDefinitions.h"
#include <windows.h>

#include <commctrl.h>
#pragma comment( lib, "comctl32.lib" )

#include <Shlobj_core.h>
#include <string>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <cassert>
#include "../include/resource.h"

HWND Main_Handler;
HWND Content_Handler;
HWND Files_Handler;
HWND PathHandler;
HWND Search_Handler;
HWND Next_Handler;
HWND Prev_Handler;

TCHAR Folder_of_Choice[MAX_PATH];
DetectiveEngine tasks;

std::vector<std::wstring> loaded_file;
std::set<int> suspisious_lines;
static unsigned int current_line{ 0 };
bool first_time_find_next{ true };
unsigned int Content_Window_Lines_Per_Page{ 0 };



void Get_Folder()
{
	const char * path_param = "";

	BROWSEINFO bi = { 0 };
	bi.lpszTitle = L"Choose a folder";
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.lpfn = NULL;
	bi.lParam = (LPARAM)path_param;

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

	if (pidl != 0)
	{
		SHGetPathFromIDList(pidl, Folder_of_Choice);
	}
}

void Display_Files()
{
	std::vector<std::wstring> display_items{ tasks.GetSuspiciousFilesList() };

	SendMessageW(Files_Handler, LB_RESETCONTENT, NULL, NULL);

	for (auto item : display_items)
	{
		int path_length = wcsnlen_s(Folder_of_Choice, MAX_PATH);
		std::wstring short_path = item.substr(path_length,MAX_PATH - path_length);
		SendMessageW(Files_Handler, LB_ADDSTRING, 0, (LPARAM)short_path.c_str());
	}
}

void RemoveExtraBackslash()
{
	int i{ 0 };

	while (Folder_of_Choice[i] != L'\0' && i < MAX_PATH)
		i++;

	if (i == 0)
		return;
	else if (Folder_of_Choice[i - 1] == L'\\')
		Folder_of_Choice[i - 1] = L'\0';
}

// Function replaces tabulations with triple-spaces.
void ReplaceTabulation(LPWSTR original_line)
{
	TCHAR new_line[MAX_LINE_LENGTH];
	int pos_new{ 0 };
	int pos_orig{ 0 };

	do
	{
		if (original_line[pos_orig] == L'\t')
		{
			for (int j = 0; j < 3; j++)
			{
				new_line[pos_new] = L' ';
				pos_new++;

				if (pos_new >= MAX_LINE_LENGTH) break;
			}
			pos_orig++;
		}
		else
		{
			new_line[pos_new] = original_line[pos_orig];
			pos_new++;
			pos_orig++;
		}
	} while (pos_new < MAX_LINE_LENGTH && original_line[pos_orig-1] != L'\0');

	new_line[MAX_LINE_LENGTH - 1] = L'\0';

	wcscpy_s(original_line, pos_new, new_line);
}

int LoadFile(LPWSTR filename)
{
	assert(wcsnlen_s(filename, MAX_PATH));

	loaded_file.clear();
	suspisious_lines.clear();

	TCHAR line[MAX_LINE_LENGTH];

	std::wifstream file{ filename };
	if (!file.is_open()) return -1;
	
	while (file.getline(line, MAX_LINE_LENGTH, '\n'))
	{
		loaded_file.push_back(line);
	}

	suspisious_lines = tasks.GetSuspiciousLinesList(filename);

	file.close();
	return 0;
}

void UpdateContentLinesPerPage()
{
	RECT content_area;
	GetClientRect(Content_Handler, &content_area);

	Content_Window_Lines_Per_Page = (content_area.bottom - CONTENT_FIRST_LINE_POSITION) / CONTENT_DISTANCE_BETWEEN_LINES;
}

void UpdateScroller()
{
	SCROLLINFO thumb;
	
	thumb.cbSize = sizeof(thumb);
	if (loaded_file.size() <= Content_Window_Lines_Per_Page)
	{
		// Trick to hide unnecessary scrollbar when number of lines is close to the window size
		thumb.fMask = SIF_PAGE | SIF_RANGE;
		thumb.nPage = Content_Window_Lines_Per_Page;
		thumb.nMin = 1;
		thumb.nMax = Content_Window_Lines_Per_Page;
	}
	else
	{
		thumb.fMask = SIF_POS | SIF_RANGE | SIF_PAGE;
		thumb.nPage = Content_Window_Lines_Per_Page;
		thumb.nMin = 0;
		thumb.nMax = loaded_file.size() - 1 + thumb.nPage / 2;
		thumb.nPos = 0;
	}

	SetScrollInfo(Content_Handler, SB_VERT, &thumb, TRUE);
}

void UpdateScrollerThumbSize()
{
	UpdateContentLinesPerPage();

	SCROLLINFO thumb;

	thumb.cbSize = sizeof(thumb);
	if (loaded_file.size() <= Content_Window_Lines_Per_Page)
	{
		// Trick to hide unnecessary scrollbar when number of lines is close to the window size
		thumb.fMask = SIF_PAGE | SIF_RANGE;
		thumb.nPage = Content_Window_Lines_Per_Page;
		thumb.nMin = 1;
		thumb.nMax = Content_Window_Lines_Per_Page;		
	}
	else
	{
		thumb.fMask = SIF_PAGE;
		thumb.nPage = Content_Window_Lines_Per_Page;
	}
	SetScrollInfo(Content_Handler, SB_VERT, &thumb, TRUE);
}

void PositionScroller()
{
	SCROLLINFO thumb;
	thumb.cbSize = sizeof(thumb);
	thumb.fMask = SIF_POS;
	thumb.nPos = current_line;
	SetScrollInfo(Content_Handler, SB_VERT, &thumb, TRUE);
}

inline void DisplayContent(unsigned int top_line_number)
{
	current_line = top_line_number;
	RedrawWindow(Content_Handler, NULL, NULL, RDW_INVALIDATE | RDW_ERASE);
}

void ResizeInterface(int width, int height)
{
	RECT main_window;
	GetWindowRect(Main_Handler, &main_window);

	// the following if statement limits the minimum size of the window
	if (main_window.right - main_window.left < MAIN_MIN_WIDTH && main_window.bottom - main_window.top < MAIN_MIN_HIEGHT)
	{
		MoveWindow(Main_Handler,
			main_window.left,
			main_window.top,
			MAIN_MIN_WIDTH,
			MAIN_MIN_HIEGHT,
			TRUE);

		RECT main_area;
		GetClientRect(Main_Handler, &main_area);
		width = main_area.right;
		height = main_area.bottom;
	}
	else if (main_window.right - main_window.left < MAIN_MIN_WIDTH)
	{
		MoveWindow(Main_Handler,
			main_window.left,
			main_window.top,
			MAIN_MIN_WIDTH,
			main_window.bottom - main_window.top,
			TRUE);

		RECT main_area;
		GetClientRect(Main_Handler, &main_area);
		width = main_area.right;
		height = main_area.bottom;
	}
	else if (main_window.bottom-main_window.top<MAIN_MIN_HIEGHT)
	{
		MoveWindow(Main_Handler,
			main_window.left,
			main_window.top,
			main_window.right - main_window.left,
			MAIN_MIN_HIEGHT,
			TRUE);

		RECT main_area;
		GetClientRect(Main_Handler, &main_area);
		width = main_area.right;
		height = main_area.bottom;
	}

	// Updating windows
	MoveWindow(Content_Handler,
		CONTENT_X_POS,
		CONTENT_Y_POS,
		width - CONTENT_X_POS - CONTENT_X_DISTANCE_TO_MAIN_RIGHT,
		height - CONTENT_Y_POS - CONTENT_Y_DISTANCE_TO_MAIN_BOTTOM,
		TRUE);

	MoveWindow(Files_Handler,
		FILES_X_POS,
		FILES_Y_POS,
		FILES_X_WIDTH,
		height - FILES_Y_POS - FILES_Y_DISTANCE_TO_MAIN_BOTTOM,
		TRUE);

	MoveWindow(Search_Handler,
		30,
		height - BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM,
		270,
		30,
		TRUE);
	
	MoveWindow(Next_Handler,
		width - 170 - NEXT_X_DISTANCE_TO_MAIN_RIGHT,
		height - BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM,
		170,
		30,
		TRUE);

	MoveWindow(Prev_Handler,
		width - 170 - PREV_X_DISTANCE_TO_MAIN_RIGHT,
		height - BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM,
		170,
		30,
		TRUE);

	//Updating the scroller of the content window
	UpdateScrollerThumbSize();
}


LRESULT CALLBACK MainProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static std::set<int>::iterator current_crime{ suspisious_lines.begin() };
	
	switch (msg)
	{
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case BROWSE_ID:
			Get_Folder();
			SetWindowTextW(PathHandler, Folder_of_Choice);
			break;

		case NEW_PATH_ID:
			GetWindowTextW(PathHandler, Folder_of_Choice, MAX_PATH);
			RemoveExtraBackslash();
			break;

		case SELECT_FILE_ID:
			if (HIWORD(wParam) == LBN_SELCHANGE)
			{
				int selected_index = (int)SendMessageW(Files_Handler, LB_GETCURSEL, NULL, NULL);

				TCHAR filename[MAX_PATH + 1];
				SendMessageW(Files_Handler, LB_GETTEXT, selected_index, (LPARAM)filename);
				
				TCHAR full_path[MAX_PATH + 1];
				wcscpy_s(full_path, MAX_PATH + 1, Folder_of_Choice);
				wcscat_s(full_path, MAX_PATH + 1, filename);

				LoadFile(full_path);
				UpdateContentLinesPerPage();
				current_line = 0;
				UpdateScroller();
				DisplayContent(0);
				first_time_find_next = true;
				current_crime = suspisious_lines.begin();
			}
			break;

		case RUN_TEST_ID:

			tasks.Clear();
			tasks.CollectTasks(Folder_of_Choice);

			tasks.InvestigateTasks();
			Display_Files();
		
			loaded_file.clear();
			DisplayContent(0);
			UpdateScroller();

			break;

		case NEXT_SUSPECT_ID:
			if (loaded_file.size() == 0) break;

			current_crime++;

			if (first_time_find_next == true)
			{
				first_time_find_next = false;
				current_crime = suspisious_lines.begin();
				current_line = *current_crime;
			}
			if (current_crime != suspisious_lines.end())
			{
				current_line = *current_crime;
			}
			else
			{
				current_crime--;
				current_line = *current_crime;
			}

			DisplayContent(current_line);

			break;

		case PREV_SUSPECT_ID:
			if (loaded_file.size() == 0) break;

			if (current_crime != suspisious_lines.begin())
			{
				current_crime--;
				current_line = *current_crime;
			}
			else
			{
				current_line = *suspisious_lines.begin();
			}

			DisplayContent(current_line);

			break;
		}
		break;

	case WM_SIZE:
		if (wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED)
			ResizeInterface(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_SIZING:
		RECT main_area;
		GetClientRect(Main_Handler, &main_area);
		ResizeInterface(main_area.right, main_area.bottom);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProcW(hwnd, msg, wParam, lParam);
}

LRESULT CALLBACK ContentProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_VSCROLL:
		switch (LOWORD(wParam))
		{
		case SB_LINEDOWN:
			if (current_line < loaded_file.size() - Content_Window_Lines_Per_Page / 2 - 1)
			{
				current_line++;
				DisplayContent(current_line);
			}
			break;
		case SB_LINEUP:
			if (current_line > 0)
			{
				current_line--;
				DisplayContent(current_line);
			}
			break;
		case SB_PAGEDOWN:
			// -1 is subtracted to keep one line from the previous page when page-up/down
			if (current_line < loaded_file.size() - Content_Window_Lines_Per_Page / 2)
			{
				current_line += Content_Window_Lines_Per_Page - 1;
				DisplayContent(current_line);
			}
			else
				current_line = loaded_file.size() - 1;
			break;
		case SB_PAGEUP:
			if (current_line > Content_Window_Lines_Per_Page - 1)
			{
				current_line -= Content_Window_Lines_Per_Page - 1;
				DisplayContent(current_line);
			}
			else
				current_line = 0;
			break;
		case SB_THUMBTRACK:
			// HIWORD(wParam) stores the corrent thumb position.

			current_line = HIWORD(wParam);
			DisplayContent(current_line);
			break;
		}
		break;
	
	case WM_MOUSEWHEEL:
	{
		int delta = GET_WHEEL_DELTA_WPARAM(wParam);
		int candidate = current_line - (delta / WHEEL_DELTA) * CONTENT_LINES_PER_DELTA_SCROLL;

		if (candidate < 0)
			current_line = 0;
		else if (candidate > loaded_file.size() - Content_Window_Lines_Per_Page / 2)
			current_line = loaded_file.size() - Content_Window_Lines_Per_Page / 2;
		else
			current_line = candidate;

		DisplayContent(current_line);
	}

		break;

	case WM_PAINT:
	{
		RECT content_area;
		GetClientRect(Content_Handler, &content_area);
		unsigned int Height_of_Content_Window = content_area.bottom;

		unsigned int pos{ CONTENT_FIRST_LINE_POSITION };
		unsigned int line_number{ current_line };
		TCHAR line[MAX_LINE_LENGTH];

		PAINTSTRUCT paintstruct;
		HDC hdc;

		hdc = BeginPaint(Content_Handler, &paintstruct);

		while (line_number < loaded_file.size() && pos < Height_of_Content_Window)
		{
			TCHAR numline[MAX_LINE_LENGTH];
			std::wstring num_str{ std::to_wstring(line_number + 1) };
			size_t length{ num_str.length() };

			bool suspicious{ false };
			if (suspisious_lines.count(line_number) > 0) suspicious = true;

			if (suspicious) SetTextColor(hdc, RGB(255, 0, 0));
			else SetTextColor(hdc, RGB(150, 150, 150));

			wcscpy_s(numline, length + 1, num_str.c_str());
			SetTextAlign(hdc, TA_RIGHT);
			TextOutW(hdc, 25, pos, numline, length);
			SetTextAlign(hdc, TA_LEFT);

			TextOutW(hdc, 27, pos, L":", 2);

			if (!suspicious) SetTextColor(hdc, RGB(0, 0, 0));

			//file.getline(line, MAX_LINE_LENGTH, '\n');
			wcscpy_s(line, MAX_LINE_LENGTH, loaded_file[line_number].c_str());
			ReplaceTabulation(line);
			length = wcsnlen_s(line, MAX_LINE_LENGTH);
			TextOutW(hdc, 40, pos, line, length);

			pos += CONTENT_DISTANCE_BETWEEN_LINES;
			line_number++;
		}

		EndPaint(Content_Handler, &paintstruct);

		PositionScroller();
	}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProcW(hwnd, msg, wParam, lParam);
}

void RegisterWindowClasses(HINSTANCE hIn)
{
	WNDCLASS wc;

	////// Registering main window /////////////
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpszClassName = L"Main_Window";
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpszMenuName = NULL;
	wc.hInstance = hIn;
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.lpfnWndProc = MainProc;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	
	RegisterClass(&wc);


	////// Registering file content window /////
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpszClassName = L"Text_Window";
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpszMenuName = NULL;
	wc.hInstance = hIn;
	wc.hbrBackground = GetSysColorBrush(COLOR_WINDOW);
	wc.lpfnWndProc = ContentProc;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClass(&wc);
}

void StartInterface(HINSTANCE hIn, int nCmdShow)
{
	RegisterWindowClasses(hIn);

	Main_Handler = CreateWindow(L"Main_Window", L"Programming crimes detector", WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_BORDER, 50, 100, 1097, 825, NULL, NULL, hIn, NULL);

	HICON icon_handler = LoadIcon(hIn, MAKEINTRESOURCEW(IDI_ICON1));
	if (icon_handler)
	{
		SendMessage(Main_Handler, WM_SETICON, ICON_BIG, (LPARAM)icon_handler);
		SendMessage(Main_Handler, WM_SETICON, ICON_SMALL, (LPARAM)icon_handler);
	}

	RECT main_area;
	GetClientRect(Main_Handler, &main_area);

	Content_Handler = CreateWindowW(L"Text_Window",
		NULL, 
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL,
		CONTENT_X_POS,
		CONTENT_Y_POS,
		main_area.right - CONTENT_X_POS - CONTENT_X_DISTANCE_TO_MAIN_RIGHT,
		main_area.bottom - CONTENT_Y_POS - CONTENT_Y_DISTANCE_TO_MAIN_BOTTOM,
		Main_Handler, 
		NULL, 
		NULL, 
		NULL);
	UpdateScroller();

	Files_Handler = CreateWindowW(WC_LISTBOXW,
		NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER | LBS_STANDARD | LBS_NOINTEGRALHEIGHT,
		FILES_X_POS,
		FILES_Y_POS,
		FILES_X_WIDTH,
		main_area.bottom - FILES_Y_POS - FILES_Y_DISTANCE_TO_MAIN_BOTTOM,
		Main_Handler,
		(HMENU)SELECT_FILE_ID,
		NULL,
		NULL);

	ShowWindow(Main_Handler, nCmdShow);
	UpdateWindow(Main_Handler);
	ShowWindow(Content_Handler, nCmdShow);
	UpdateWindow(Content_Handler);
	ShowWindow(Files_Handler, nCmdShow);
	UpdateWindow(Files_Handler);

	CreateWindowW(L"static", L"Suspicious files ", WS_CHILD | WS_VISIBLE, 33, 95, 200, 25, Main_Handler, NULL, NULL, NULL);
	CreateWindowW(L"static", L"Suspicious code ", WS_CHILD | WS_VISIBLE, 400, 25, 200, 25, Main_Handler, NULL, NULL, NULL);
	CreateWindowW(L"static", L"Working folder ", WS_CHILD | WS_VISIBLE, 33, 25, 100, 25, Main_Handler, NULL, NULL, NULL);

	CreateWindowW(L"Button", L"Browse...", WS_VISIBLE | WS_CHILD | WS_BORDER, 300, 50, 80, 30, Main_Handler, (HMENU)BROWSE_ID, NULL, NULL);
	
	Search_Handler = CreateWindowW(L"Button", 
		L"Search for programming crimes", 
		WS_VISIBLE | WS_CHILD | WS_BORDER, 
		30, 
		main_area.bottom-BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM, 
		270, 
		30, 
		Main_Handler, 
		(HMENU)RUN_TEST_ID, 
		NULL, 
		NULL);
	
	Next_Handler = CreateWindowW(L"Button", 
		L"Next suspect", 
		WS_VISIBLE | WS_CHILD | WS_BORDER, 
		main_area.right - 170 - NEXT_X_DISTANCE_TO_MAIN_RIGHT,
		main_area.bottom - BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM, 
		170, 
		30, 
		Main_Handler, 
		(HMENU)NEXT_SUSPECT_ID, 
		NULL, 
		NULL);
	
	Prev_Handler = CreateWindowW(L"Button",
		L"Previous suspect",
		WS_VISIBLE | WS_CHILD | WS_BORDER,
		main_area.right - 170 - PREV_X_DISTANCE_TO_MAIN_RIGHT,
		main_area.bottom - BUTTONS_Y_DISTANCE_TO_MAIN_BOTTOM,
		170,
		30,
		Main_Handler,
		(HMENU)PREV_SUSPECT_ID,
		NULL,
		NULL);

	PathHandler = CreateWindowW(L"Edit", NULL, WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOHSCROLL, 30, 50, 260, 30, Main_Handler, (HMENU)NEW_PATH_ID, NULL, NULL);
}
