#include "..\include\FSA.h"
#include "..\include\PCrimesList.h"

bool GotoFSA::InputToken(const std::string & token)
{
	return token == "goto";
}


bool VLAandGlobalFSA::InputToken(const std::string & token, Token_Type type, PCrime_Type &crime_type)
{
	if (token == "{")
		opened_curly++;
	else if (token == "}")
		opened_curly--;

	switch (state_FSA)
	{
	case FSA_State::INIT:
		if (type == Token_Type::TYPENAME || type == Token_Type::IDENTIFIER)
			state_FSA = FSA_State::TYPE_NAME;
		break;

	case FSA_State::TYPE_NAME:
		if (type == Token_Type::IDENTIFIER)
			state_FSA = FSA_State::VAR_NAME;
		else if (type == Token_Type::TYPENAME)
			state_FSA = FSA_State::TYPE_NAME;
		else if (token == "::")
			state_FSA = FSA_State::INIT;
		else if (token == "<")
			state_FSA = FSA_State::ANGLE_BR_OPENED;
		else if (token == "(")
			state_FSA = FSA_State::MANY_IN_PARENTH;
		else
			state_FSA = FSA_State::INIT;
		break;

	case FSA_State::VAR_NAME:
		if (token == "[")
			state_FSA = FSA_State::SQUARE_BR_OPENED;
		else if (token == "(")
			state_FSA = FSA_State::PARENTH_OPENED;
		else if (token == "::")
			state_FSA = FSA_State::TYPE_NAME;
		else
		{
			state_FSA = FSA_State::INIT;
			if (opened_curly == 0)
				return true;
		}
		break;

	case FSA_State::SQUARE_BR_OPENED:
		if (type == Token_Type::IDENTIFIER)
		{
			state_FSA = FSA_State::INIT;
			crime_type = PCrime_Type::VLA;
			return true;
		}
		else
			state_FSA = FSA_State::INIT;
		break;

	case FSA_State::ANGLE_BR_OPENED:
		if (token == ">")
			state_FSA = FSA_State::TYPE_NAME;
		else if (type == Token_Type::TYPENAME ||
			type == Token_Type::IDENTIFIER ||
			token == "," || token == "::")
			state_FSA = FSA_State::ANGLE_BR_OPENED;
		else
			state_FSA = FSA_State::INIT;

		break;

	case FSA_State::PARENTH_OPENED:
		if (token == ")")
			state_FSA = FSA_State::INIT;
		else if (token[0] >= '0' && token[0] <= '9')
		{
			state_FSA = FSA_State::INIT;
			if (opened_curly == 0)
			{
				crime_type = PCrime_Type::GLOBAL;
				return true;
			}
		}
		else
			state_FSA = FSA_State::SMTHG_IN_PARENTH;
		break;

	case FSA_State::SMTHG_IN_PARENTH:
		if (token == ")")
		{
			state_FSA = FSA_State::INIT;
			if (opened_curly == 0)
			{
				crime_type = PCrime_Type::GLOBAL;
				return true;
			}
		}
		else if (token == "::")
			state_FSA = FSA_State::PARENTH_OPENED;
		else
			state_FSA = FSA_State::MANY_IN_PARENTH;
		break;

	case FSA_State::MANY_IN_PARENTH:
		if (token == ")")
			state_FSA = FSA_State::INIT;
		break;
	}

	return false;
}

void VLAandGlobalFSA::Reset()
{
	state_FSA = FSA_State::INIT;
	opened_curly = 0;
}