#include "../include/PCrime.h"

#include <string>

PCrime::PCrime(PCrime_Type n_type, int n_line) :
	type(n_type), found_in_line(n_line) {};


PCrime_Type PCrime::Get_Type() const
{
	return type;
}


int PCrime::Get_Line() const
{
	return found_in_line;
}


void PCrime::Set_PCrime(PCrime_Type n_type, int n_line)
{
	type = n_type;
	found_in_line = n_line;
}


std::ostream & operator<< (std::ostream & str, const PCrime & rhs)
{
	if (rhs.type == PCrime_Type::GOTO) str << "GOTO\n";
	else if (rhs.type == PCrime_Type::GLOBAL) str << "GLOBAL\n";
	else if (rhs.type == PCrime_Type::VLA) str << "VLA\n";

	str << "Line : " << rhs.found_in_line;
	str << "\nNo veriable name\n\n";

	return str;
}