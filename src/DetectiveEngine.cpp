#include"../include/DetectiveEngine.h"

#include<strsafe.h>
#include<string>
#include<map>
#include<set>
#include<fstream>
#include<thread>
#include<cassert>

DetectiveEngine::DetectiveEngine()
{
	max_num_of_threads = std::thread::hardware_concurrency() - NUM_OF_RESERVED_THREADS;
	if (max_num_of_threads < 1) max_num_of_threads = 1;
}

int DetectiveEngine::CollectTasks(LPCWSTR folder)
{
	TCHAR search_request[MAX_PATH + 4];
	StringCchCopyW(search_request, MAX_PATH + 4, folder);
	StringCchCatW(search_request, MAX_PATH + 4, L"\\*.*");


	TCHAR path[MAX_PATH + 4];
	StringCchCopyW(path, MAX_PATH + 4, folder);
	StringCchCatW(path, MAX_PATH + 4, L"\\");


	WIN32_FIND_DATAW file;
	HANDLE search_handle = FindFirstFileW(search_request, &file);

	if (search_handle == INVALID_HANDLE_VALUE)
		return 1;

	do
	{
		if (CheckFilename(&file) == FileTypes::FOLDER)
		{
			TCHAR next_path[MAX_PATH + 4];
			StringCchCopyW(next_path, MAX_PATH + 4, path);
			StringCchCatW(next_path, MAX_PATH + 4, file.cFileName);
			
			CollectTasks(next_path);
		}
		else if (CheckFilename(&file) == FileTypes::INTERESTING)
		{
			TCHAR full_path[MAX_PATH + 4];
			StringCchCopyW(full_path, MAX_PATH + 4, path);
			StringCchCatW(full_path, MAX_PATH + 4, file.cFileName);

			files.push_back(full_path);
		}
	} while (FindNextFileW(search_handle, &file));

	FindClose(search_handle);

	return 0;
}

void DetectiveEngine::InvestigateTasks()
{
	std::vector<std::thread> workers;

	current_job_index = 0;

	for (int i = 0; i < max_num_of_threads; i++) workers.push_back(std::thread(&DetectiveEngine::WorkerThreadFunction, std::ref(*this)));

	for (int i = 0; i < max_num_of_threads; i++) workers[i].join();
}

void DetectiveEngine::LogResultToFile(std::string filename) const
{
	std::wofstream ofile{filename};

	for (auto item : found_crimes_storage)
	{
		ofile << "In_file_______" << item.first << '\n';
		ofile << item.second <<'\n';
	}

}

int DetectiveEngine::GetNumberOfTasks() const
{
	return files.size();
}

std::vector<std::wstring> DetectiveEngine::GetSuspiciousFilesList() const
{
	std::vector<std::wstring> result;
	result.reserve(found_crimes_storage.size());

	for (auto crime : found_crimes_storage)
	{
		result.push_back(crime.first);
	}

	return result;
}

std::set<int> DetectiveEngine::GetSuspiciousLinesList(std::wstring filename) const
{
	return found_crimes_storage.at(filename).GetCrimeLinesNumbers();
}

void DetectiveEngine::Clear()
{
	files.clear();
	found_crimes_storage.clear();
}


DetectiveEngine::FileTypes DetectiveEngine::CheckFilename(LPWIN32_FIND_DATAW file)
{
	if (file->dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
	{
		if (wcsncmp(file->cFileName,L".",2)!=0 && wcsncmp(file->cFileName,L"..",3)!=0)
		{
			return FileTypes::FOLDER;
		}
		else
			return FileTypes::UNINTERESTING;
	}
	
	std::wstring filename = file->cFileName;
	int filename_length = filename.length();

	if (filename_length < 3)
		return FileTypes::UNINTERESTING;

	if (filename.rfind(L".c") == filename.length() - 2) // This is the check if the filename ENDS with .c
		return FileTypes::INTERESTING;
	if (filename.rfind(L".cc") == filename.length() - 3)
		return FileTypes::INTERESTING;
	if (filename.rfind(L".h") == filename.length() - 2)
		return FileTypes::INTERESTING;


	if (filename_length < 4)
		return FileTypes::UNINTERESTING;

	if (filename.rfind(L".cpp") == filename.length() - 4)
		return FileTypes::INTERESTING;
	if (filename.rfind(L".hpp") == filename.length() - 4)
		return FileTypes::INTERESTING;
	
	return FileTypes::UNINTERESTING;
}

void DetectiveEngine::SingleFileAnalysis(std::wstring filename)
{
	PCrimesList crimes{ filename };

	if (crimes.GetNumberOfCrimes() != 0)
	{
		assert(found_crimes_storage.count(filename) == 0);
		
		std::lock_guard<std::mutex> lock{ storage_lock };
		found_crimes_storage.insert(std::pair<std::wstring, PCrimesList>(filename, std::move(crimes)));
	}
}

void DetectiveEngine::WorkerThreadFunction()
{
	std::wstring filename;

	while (true)
	{
		{
			std::lock_guard<std::mutex> permission(current_job_lock);
			if (current_job_index < files.size())
			{
				filename = files[current_job_index];
				current_job_index++;
			}
			else
				return;
		}

		SingleFileAnalysis(filename);
		//SendMessage(Progress_Handler, PBM_STEPIT, 0, 0);
	}
}