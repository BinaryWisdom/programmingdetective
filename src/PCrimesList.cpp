#include "../include/PCrimesList.h"
#include "../include/FSA.h"
#include <fstream>
#include <iostream>
#include <cassert>
#include <string>
#include <set>
#include <regex>


PCrimesList::PCrimesList(const PCrimesList &origin) : crimes(origin.crimes) 
{
	Initialize();
}


PCrimesList::PCrimesList(PCrimesList &&origin)
{
	std::swap(origin.crimes, crimes);
	Initialize();
}


PCrimesList & PCrimesList::operator=(const PCrimesList &rhs)
{
	crimes = rhs.crimes;
	return *this;

}


PCrimesList & PCrimesList::operator=(PCrimesList &&rhs)
{
	std::swap(crimes, rhs.crimes);
	return *this;
}


PCrimesList::PCrimesList(const std::wstring & filename)
{
	Initialize();
	FindCrimesInFile(filename);
}


// FindCrimesInFile looks for PCrimes in a given file and stores them in 
// the PCrimesList. It returns the number of found PCrimes if the function 
// completed successfully and -1 if it failed to open the file. 
int PCrimesList::FindCrimesInFile(const std::wstring & filename)
{
	line_number = 0;
	opened_curly = 0;

	GotoFSA gotoFSA;
	VLAandGlobalFSA vlaGlobalFSA;

	std::ifstream input_file(filename);

	if (input_file.fail())
	{
		//	std::cerr << "ERROR : failed to open " << filename << " !!!\n";
		return -1;
	}

	std::string token;
	PCrime_Type crime_type;

	while (input_file.good())
	{
		token = Next_Token(input_file);
		if (token.length() == 0)
			continue;
		else if (token == "typedef")
			SkipCurrentStatement(input_file);

		Token_Type type{ Get_Token_Type(token) };

		if (vlaGlobalFSA.InputToken(token, type, crime_type))
		{
			crimes.push_back(PCrime(crime_type, line_number));
		}
		else if (gotoFSA.InputToken(token))
		{
			crimes.push_back(PCrime(PCrime_Type::GOTO, line_number));
		}
	}

	input_file.close();
	return 0;
}


int PCrimesList::GetNumberOfCrimes() const
{
	return crimes.size();
}


std::set<int> PCrimesList::GetCrimeLinesNumbers() const
{
	std::set<int> result;

	for (auto crime : crimes)
		result.insert(crime.Get_Line());

	return result;
}


Token_Type PCrimesList::Get_Token_Type(const std::string & token) const
{
	if (datatypes.count(token) > 0)
		return Token_Type::TYPENAME;
	else if (std::regex_match(token, std::regex("[_[:alpha:]]+[_[:alnum:]]*")) and
		non_datatypes_keywords.count(token)==0)
		return Token_Type::IDENTIFIER;
	else
		return Token_Type::OTHER;
}


inline std::string PCrimesList::Next_Token(std::ifstream & input_file)
{
	assert(input_file.is_open());

	char next_symbol;
	input_file >> std::noskipws;

	input_file >> next_symbol;

	while (input_file.good() && (next_symbol == ' ' || next_symbol == '\t'))	// skip whitespaces
	{
		input_file >> next_symbol;
	}
	
	if (next_symbol == '/' and input_file.peek() == '/')	// ignore one-line comment
	{
		while (input_file.good() && input_file.peek() != '\n')
			input_file.ignore(1);

		if (input_file.peek() == '\n')
		{
			input_file.ignore(1);
			line_number++;
		}
		
		return "";
	}
	
	if (next_symbol == '/' and input_file.peek() == '*')	// ignore multiline comment
	{
		input_file.ignore(1);

		do
		{
			input_file >> next_symbol;
			if (next_symbol == '\n') line_number++;
		} while (input_file.good() && (next_symbol != '*' || input_file.peek() != '/'));

		if (input_file.good()) input_file.ignore(1);
		return "";
	}

	if (next_symbol == '#')	// ignore preprocessor directives
	{
		while (input_file.good() && input_file.peek() != '\n')
			input_file.ignore(1);
		
		if (input_file.peek() == '\n')
		{
			input_file.ignore(1);
			line_number++;
		}

		return "";
	}

	if (next_symbol == '(' && opened_curly > 0)	// ignore ( ) inside { }
	{
		int opened_parenth{ 1 };

		while (opened_parenth != 0)
		{
			if (input_file.peek() == '(')
				opened_parenth++;
			else if (input_file.peek() == ')')
				opened_parenth--;

			input_file.ignore(1);
		}

		return "";
	}

	if (next_symbol == '<' && input_file.peek()!='<' && opened_curly == 0)	// ignore < > outside { }
	{
		int opened_angle{ 1 };

		while (input_file.good() && opened_angle != 0)
		{
			if (input_file.peek() == '<')
				opened_angle++;
			else if (input_file.peek() == '>')
				opened_angle--;

			input_file.ignore(1);
		}

		return "";
	}

	if (next_symbol == '\n')
	{
		line_number++;
		return "";
	}

	std::string token;
	token = next_symbol;

	if (!input_file.good())
	{
		return token;
	}

	if (next_symbol == '{')
	{
		opened_curly++;
		return token;
	}

	if (next_symbol == '}')
	{
		opened_curly--;
		return token;
	}

	if (next_symbol == '[' || next_symbol == ']' ||
		next_symbol == '(' || next_symbol == ')' ||
		next_symbol == '<' || next_symbol == '>' ||
		next_symbol == '*' || next_symbol == '&' ||
		next_symbol == ';')
	{
		return token;
	}

	if(next_symbol == '=')
	{
		if (input_file.peek() == '=')
		{
			input_file.ignore(1);
			return "==";
		}
		else
			return "=";
	}

	if (next_symbol == ':')
	{
		if (input_file.peek() == ':')
		{
			input_file.ignore(1);
			return "::";
		}
		else
			return ":";
	}


	if (next_symbol == '\"')	// string literal
	{
		do
		{
			input_file >> next_symbol;

			if (next_symbol != '\n')
				token += next_symbol;
			else
				line_number++;

			if (next_symbol == '\\')
			{
				input_file >> next_symbol;
				token += next_symbol;

				input_file >> next_symbol;
				if (next_symbol != '\n')
					token += next_symbol;
				else
					line_number++;

			}
		} while (input_file.good() && next_symbol != '\"');

		return token;
	}

	if (next_symbol == '\'')	// character literal
	{
		if (input_file.peek() == '\\')
		{
			input_file >> next_symbol;
			token += next_symbol;
		}

		input_file >> next_symbol;
		token += next_symbol;
		input_file >> next_symbol;
		token += next_symbol;

		return token;
	}

	while (separators.count(input_file.peek())==0 or (token.length()>=8 and token.compare(token.length()-8,8,"operator")==0))
	{
		input_file >> next_symbol;
		token += next_symbol;
	}

	return token;
}


inline void PCrimesList::SkipCurrentStatement(std::ifstream & input_file)
{
	int local_curly{ 0 };
	
	while (input_file.good() && (input_file.peek() != ';' || local_curly != 0))
	{
		if (input_file.peek() == '{')
			local_curly++;
		else if (input_file.peek() == '}')
			local_curly--;

		input_file.ignore(1);
	}
}


void PCrimesList::Initialize()
{
	datatypes.emplace("bool");

	datatypes.emplace("char");
	datatypes.emplace("wchar_t");
	datatypes.emplace("char16_t");
	datatypes.emplace("char32_t");

	datatypes.emplace("short");
	datatypes.emplace("int");
	datatypes.emplace("long");
	datatypes.emplace("signed");
	datatypes.emplace("unsigned");

	datatypes.emplace("float");
	datatypes.emplace("double");

	datatypes.emplace("void");
	datatypes.emplace("auto");

	datatypes.emplace("string");
	datatypes.emplace("basic_string");
	datatypes.emplace("u16string");
	datatypes.emplace("u32string");
	datatypes.emplace("wstring");
	datatypes.emplace("vector");
	datatypes.emplace("array");
	datatypes.emplace("valarray");
	datatypes.emplace("list");
	datatypes.emplace("forward_list");
	datatypes.emplace("stack");
	datatypes.emplace("queue");
	datatypes.emplace("priority_queue");
	datatypes.emplace("deque");
	datatypes.emplace("map");
	datatypes.emplace("multimap");
	datatypes.emplace("set");
	datatypes.emplace("unordered_multiset");
	datatypes.emplace("unordered_map");
	datatypes.emplace("unordered_multimap");
	datatypes.emplace("unordered_set");
	datatypes.emplace("unordered_multiset");
	datatypes.emplace("bitset");

	datatypes.emplace("iterator");
	datatypes.emplace("reverse_iterator");
	datatypes.emplace("const_iterator");
	datatypes.emplace("const_reverse_iterator");


	non_datatypes_keywords.emplace("alignas");
	non_datatypes_keywords.emplace("alignof");
	non_datatypes_keywords.emplace("and");
	non_datatypes_keywords.emplace("and_eq");
	non_datatypes_keywords.emplace("asm");
	non_datatypes_keywords.emplace("bitand");
	non_datatypes_keywords.emplace("bitor");
	non_datatypes_keywords.emplace("break");
	non_datatypes_keywords.emplace("case");
	non_datatypes_keywords.emplace("catch");
	non_datatypes_keywords.emplace("class");
	non_datatypes_keywords.emplace("compl");
	non_datatypes_keywords.emplace("concept");
	non_datatypes_keywords.emplace("const");
	non_datatypes_keywords.emplace("consteval");
	non_datatypes_keywords.emplace("constexpr");
	non_datatypes_keywords.emplace("const_cast");
	non_datatypes_keywords.emplace("continue");
	non_datatypes_keywords.emplace("co_await");
	non_datatypes_keywords.emplace("co_return");
	non_datatypes_keywords.emplace("co_yield");
	non_datatypes_keywords.emplace("decltype");
	non_datatypes_keywords.emplace("default");
	non_datatypes_keywords.emplace("delete");
	non_datatypes_keywords.emplace("do");
	non_datatypes_keywords.emplace("dynamic_cast");
	non_datatypes_keywords.emplace("else");
	non_datatypes_keywords.emplace("enum");
	non_datatypes_keywords.emplace("explicit");
	non_datatypes_keywords.emplace("export");
	non_datatypes_keywords.emplace("extern");
	non_datatypes_keywords.emplace("false");
	non_datatypes_keywords.emplace("for");
	non_datatypes_keywords.emplace("friend");
	non_datatypes_keywords.emplace("got");
	non_datatypes_keywords.emplace("if");
	non_datatypes_keywords.emplace("inline");
	non_datatypes_keywords.emplace("mutable");
	non_datatypes_keywords.emplace("namespace");
	non_datatypes_keywords.emplace("new");
	non_datatypes_keywords.emplace("noexcept");
	non_datatypes_keywords.emplace("not");
	non_datatypes_keywords.emplace("not_eq");
	non_datatypes_keywords.emplace("nullptr");
	non_datatypes_keywords.emplace("operator");
	non_datatypes_keywords.emplace("or");
	non_datatypes_keywords.emplace("or_eq");
	non_datatypes_keywords.emplace("private");
	non_datatypes_keywords.emplace("protected");
	non_datatypes_keywords.emplace("public");
	non_datatypes_keywords.emplace("register");
	non_datatypes_keywords.emplace("reinterpret_cast");
	non_datatypes_keywords.emplace("requires");
	non_datatypes_keywords.emplace("return");
	non_datatypes_keywords.emplace("sizeof");
	non_datatypes_keywords.emplace("static");
	non_datatypes_keywords.emplace("static_assert");
	non_datatypes_keywords.emplace("static_cast");
	non_datatypes_keywords.emplace("struct");
	non_datatypes_keywords.emplace("switch");
	non_datatypes_keywords.emplace("template");
	non_datatypes_keywords.emplace("this");
	non_datatypes_keywords.emplace("thread_local");
	non_datatypes_keywords.emplace("throw");
	non_datatypes_keywords.emplace("true");
	non_datatypes_keywords.emplace("try");
	non_datatypes_keywords.emplace("typedef");
	non_datatypes_keywords.emplace("typeid");
	non_datatypes_keywords.emplace("typename");
	non_datatypes_keywords.emplace("union");
	non_datatypes_keywords.emplace("using");
	non_datatypes_keywords.emplace("virtual");
	non_datatypes_keywords.emplace("volatile");
	non_datatypes_keywords.emplace("while");
	non_datatypes_keywords.emplace("xor");
	non_datatypes_keywords.emplace("xor_eq");

	separators.emplace('=');
	separators.emplace(';');
	separators.emplace(':');
	separators.emplace('{');
	separators.emplace('}');
	separators.emplace('[');
	separators.emplace(']');
	separators.emplace('<');
	separators.emplace('>');
	separators.emplace(' ');
	separators.emplace('\t');
	separators.emplace('\n');
	separators.emplace('(');
	separators.emplace(')');
	separators.emplace(EOF);
}

 
std::ostream & operator<< (std::ostream & str, const PCrimesList & rhs)
{
	for (auto crime : rhs.crimes)
	{
		str << crime.Get_Line() << " : ";
		if (crime.Get_Type() == PCrime_Type::GOTO) str << "GOTO\n";
		else if (crime.Get_Type() == PCrime_Type::GLOBAL) str << "GLOBAL\n";
		else if (crime.Get_Type() == PCrime_Type::VLA) str << "VLA\n";
	}
	str << '\n';

	return str;
}


std::wostream & operator<< (std::wostream & str, const PCrimesList & rhs)
{
	for (auto crime : rhs.crimes)
	{
		str << crime.Get_Line() << " : ";
		if (crime.Get_Type() == PCrime_Type::GOTO) str << "GOTO\n";
		else if (crime.Get_Type() == PCrime_Type::GLOBAL) str << "GLOBAL\n";
		else if (crime.Get_Type() == PCrime_Type::VLA) str << "VLA\n";
	}
	str << '\n';

	return str;
}